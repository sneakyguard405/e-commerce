const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  userId: Number,
  totalPrice: Number,
  productQty: Number,
  isCancel: {type: Boolean, default: false},
  products:[
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "products"
    }
  ],
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()}
});
schema.plugin(AutoIncrement, {inc_field: 'orderId'});
module.exports = mongoose.model('orders', schema, 'orders');
