const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  nameF: String,
  nameL: String,
  address: String,
  mobile: String,
  email: String,
  password: String,
  profileImage: String,
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()}
});
schema.plugin(AutoIncrement, {inc_field: 'userId'});
module.exports = mongoose.model('Users', schema);
