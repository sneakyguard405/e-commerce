const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const schema = mongoose.Schema({
  productName: String,
  productImage: String,
  productDetail: String,
  productPrice: Number,
  isActive: {type: Boolean, default: true},
  createdAt: {type: Date, default: Date.now()},
  updatedAt: {type: Date, default: Date.now()}
});
schema.plugin(AutoIncrement, {inc_field: 'productId'});
module.exports = mongoose.model('products', schema, 'products');
