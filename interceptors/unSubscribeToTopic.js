
const admin = require('firebase-admin');
const serviceAccount = require('../Api/config/serviceaccount.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  })

const unSubscribeToTopic = (topic, deviceId)=>{
    admin.messaging().unsubscribeFromTopic(deviceId, topic)
    .then((response) => {
      //console.log('Successfully unSubscribed to topic:', response);
    })
    .catch((error) => {
      //console.log('Error Unsubscribing to topic:', error);
    });
}

module.exports ={
    unSubscribeToTopic
}