
const interceptor1 = (req, res, next) => {
  if(req.query.token1 == '1234') {
    next();
  }else{
    res.end('invalid token1');
  }
}

const interceptor2 = (req, res, next) => {
  if(req.query.token2 == '4321') {
    next();
  }else{
    res.end('invalid token2');
  }
}

module.exports = {
  interceptor1,
  interceptor2
}
