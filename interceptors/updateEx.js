

const Member = require('../models/memberSchema');
const logPurchese = require('../models/logPurchese');
const Purchase = require('../models/purchese');

const updateStatusMember = async ()=>{
    let memberData = await Member.aggregate([
      { $match : { $and: [ {"expiredDate":{"$lte":new Date()}}, {topFanStatus: true}] }},
      {
        "$lookup": {
            "from": "request_approves",
            "let": {
                "member_ids": "$member_id"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$eq": ["$$member_ids", "$memberId"]
                        },
                        // "$or":[{purchaseStatus:0}, {purchaseStatus:1}, {purchaseStatus:2}],
                        "$and":[{approveType:1},  {purchaseStatus:1}],
                    }
                },
                {
                    "$sort": {
                        "createdAt": - 1
                    }
                }
            ],
            "as": "purchaseInfo"
        }
    } 
      ]);
    // let memberData = await Member.find({ $and: [ {"expiredDate":{"$lte":new Date('2021-07-26')}}, {topFanStatus: true}] });
    return memberData
  }

    async function updateExpire(){
        let members = await updateStatusMember();
        let data = []
        let memId = []
        let deviceId = []
          members.map(async (row, index)=>{
            memId[index] = row.member_id;
            deviceId.push(row.deviceId)
            let x = 0
            if(row.purchaseInfo[0]){
              let purInfo = row.purchaseInfo[0]
              data[x] = {
                requestId:parseInt(purInfo.request_approve_id),
                approveType:1,
                memberId:purInfo.memberId,
                memberNo:purInfo.memberNo,
                userAction:0,
                packageId:purInfo.packageId,
                oldStatus:purInfo.purchaseStatus,
                status:6,
              }
              x += 1
            }
          })

     
            if(deviceId.length > 0){
                subscribeToTopic('gest', deviceId)
                unSubscribeToTopic('topfan', deviceId)
             }
    

         await Purchase.update({$and:[{memberId: {$in: memId}}, {purchaseStatus: 1}, {approveType: 1}]}, {purchaseStatus: 5})
         await Member.updateMany({member_id: {$in: memId}}, { topFanStatus: false })
         await logPurchese.insertMany(data)
  }

  module.exports ={
    updateExpire
}

  


