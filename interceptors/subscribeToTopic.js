
const admin = require('firebase-admin');


const subscribeToTopic = (topic, deviceId) =>{
    admin.messaging().subscribeToTopic([`${deviceId}`], topic)
    .then((response) => {
      //console.log(`Successfully subscribed to topic from open APP Device ID is :: ${deviceId}`, response);
    })
    .catch((error) => {
      //console.log('Error subscribing to topic:', error);
    });
}

const unSubscribeToTopic = (topic, deviceId)=>{
    admin.messaging().unsubscribeFromTopic(deviceId, topic)
    .then((response) => {
      // console.log('Successfully unSubscribed to topic: ' + topic +" ", response);
    })
    .catch((error) => {
      // console.log('Error Unsubscribing to topic:', error);
    });

    // .then((response) => {
    //   console.log(`Successfully subscribed to topic from open APP Device ID is :: ${device_id}`, response);
    // })
    // .catch((error) => {
    //   console.log('Error subscribing to topic:', error);
    // });
}

module.exports ={
    subscribeToTopic,
    unSubscribeToTopic
}
