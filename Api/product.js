const express = require("express");
const router = express.Router();
const Product = require("../models/productSchema");

router.get("/product/:productId?", async (req, res) => {
  const { productId } = req.params;
  const condition = { isActive: true };
  if (productId) condition.productId = parseInt(productId);
  const products = await Product.find(condition);
  return res.json(products);
});

router.get("/product/:productId", async (req, res) => {
  const products = await Product.findOne({ isActive: true });
  return res.json(products);
});

module.exports = router;
