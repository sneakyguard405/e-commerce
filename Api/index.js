const express = require('express');
const router = express.Router();
const jwt = require('./auth/jwt');
require('./../config/db');

router.use(require('./auth/auth'));
router.use(require('./product'));

router.use(jwt.verify);
router.use(require('./user'));
router.use(require('./management/product'));
router.use(require('./order'));
module.exports = router;
