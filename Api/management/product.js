const express = require("express");
const router = express.Router();
const Product = require("../../models/productSchema");
const fileSystem = require("../utils/uploadFile");
const { body, validationResult } = require("express-validator");


router.post(
  "/product",
  body("name").not().isEmpty().trim().escape(),
  body("price").not().isEmpty().trim().escape(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, detail, price, isActive } = req.body;
    let productImages = "";
    if (req.files) {
      const targetFile = req.files.img;
      productImages = await fileSystem.uploadFile(targetFile, "productImages");
    }

    const dataProduct = {
      productName: name,
      productImage: productImages,
      productDetail: detail,
      productPrice: parseInt(price),
      isActive: isActive === "true" || isActive === true,
    };
    await Product.create(dataProduct);
    return res.json({ result: "ok", message: { name } });
  }
);

router.put(
  "/product/:productId",
  body("name").not().isEmpty().trim().escape(),
  body("price").not().isEmpty().trim().escape(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, detail, price, isActive } = req.body;
    const { productId } = req.params;
    let productImages = "";
    if (req.files) {
      const targetFile = req.files.img;
      productImages = await fileSystem.uploadFile(targetFile, "productImages");
    }

    const dataProduct = {
      productName: name,
      productImage: productImages,
      productDetail: detail,
      productPrice: parseInt(price),
      isActive: isActive === "true" || isActive === true,
    };

    if (productImages === "") {
      delete dataProduct.productImage;
    }

    await Product.findOneAndUpdate({ productId }, dataProduct);
    return res.json({ result: "ok", message: { name } });
  }
);

router.delete(
  "/product/:productId",
  async (req, res) => {
    const { productId } = req.params;
    const deleteS = await Product.findOneAndDelete({productId});
    if(deleteS){
      return res.json({ result: "delete success", message: { deleteS } });
    }else{
      return res.json({ result: "delete unsuccess"  });
    }
  }
);

module.exports = router;
