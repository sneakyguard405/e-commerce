const express = require("express");
const router = express.Router();
const Product = require("../models/productSchema");
const Order = require("../models/orderSchema");
const { body, validationResult } = require("express-validator");
const jwt_decode = require("jwt-decode");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

router.post("/order", async (req, res) => {
  const { productId } = req.body;
  if (!Array.isArray(productId)) {
    return res.json("productId isn't array");
  } else {
    for (let i = 0; i < productId.length; i++) {
      productId[i] = ObjectId(productId[i]);
    }
  }

  let token = req.header("authorization").split(" ")[1];
  let decodedToken = jwt_decode(token);

  let totalPrice = await Product.aggregate([
    { $match: { _id: { $in: productId } } },
    { $group: { _id: null, TotalSum: { $sum: "$productPrice" } } },
  ]);

  const dataOrder = {
    userId: parseInt(decodedToken.userId),
    productQty: productId.length,
    totalPrice: totalPrice[0].TotalSum,
    products: productId,
    isCancel: false,
  };

  const orders = await Order.create(dataOrder);
  if (orders) {
    return res.json(orders);
  } else {
    return res.json("insert not success");
  }
});

router.get("/order/:orderId?", async (req, res) => {
  const { orderId } = req.params;
  const condition = {}; 
  if (orderId) condition.orderId = parseInt(orderId);
  const orders = await Order.find(condition).populate("products");
  return res.json(orders);
});

router.post("/cancelOrder/:orderId", async (req, res) => {
  const { orderId } = req.params;
  const orders = await Order.findOneAndUpdate({orderId: parseInt(orderId)}, {isCancel: true});
  if(orders){
    return res.json("update success");
  }else{
    return res.json("update unsuccess");
  }
});

module.exports = router;
