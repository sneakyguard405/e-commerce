const express = require("express");
const router = express.Router();
const jwt_decode = require('jwt-decode');
const Users = require("../models/userSchema");

router.get("/my-profile", async (req, res) => {
  let token = req.header('authorization').split(" ")[1]
  let decodedToken = jwt_decode(token);
  let profile = await Users.findOne({userId: decodedToken.userId});
  return res.json( profile );
});

module.exports = router;
