const express = require("express");
const randtoken = require("rand-token");
const bcrypt = require("bcryptjs");
const { body, validationResult } = require("express-validator");
const jwt = require("./jwt");
const fileSystem = require("../utils/uploadFile");
const Users = require("./../../models/userSchema");

const router = express.Router();
const refreshTokens = {};

router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  const doc = await Users.findOne({ email: email });

  if (doc) {
    isValidPassword = await bcrypt.compare(password, doc.password);
    if (isValidPassword) {
      const payload = {
        id: doc._id,
        email: doc.email,
        userId: doc.userId,
      };

      const token = jwt.sign(payload, "200000000"); // 20000 sec
      const refreshToken = randtoken.uid(256);
      refreshTokens[refreshToken] = email;
      res.json({
        result: "ok",
        token,
        refreshToken,
        message: "Login successfully",
      });
    } else {
      res.json({
        result: "nok",
        message: "Invalid password",
      });
    }
  } else {
    res.json({
      result: "nok",
      message: "Invalid email",
    });
  }
});

router.post(
  "/register",
  body("email").isEmail(),
  body("password").isLength({ min: 5 }),
  body("nameF").not().isEmpty().trim().escape(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const { password, email, nameF, nameL, address, mobile } = req.body;
      const userData = await Users.findOne({ email: email });
      if (userData) {
        return res
          .status(400)
          .json({
            errors: [
              {
                value: email,
                msg: "email alreay in use",
                param: "email",
                location: "body",
              },
            ],
          });
      }
      let profileImage = "";
      if (req.files) {
        const targetFile = req.files.img;
        profileImage = await fileSystem.uploadFile(targetFile, "userImages");
      }

      const dataInsert = {
        password: await bcrypt.hash(password, 8),
        email,
        nameF,
        nameL,
        address,
        mobile,
        profileImage,
      };

      await Users.create(dataInsert);
      return res.json({ result: "ok", message: { nameF, nameL } });
    } catch (e) {
      return res.json({ result: "nok", message: e });
    }
  }
);

// Refresh Token
let count = 1;
router.post("/refresh/token", function (req, res) {
  const refreshToken = req.body.refreshToken;
  if (refreshToken in refreshTokens) {
    const payload = {
      username: refreshTokens[refreshToken],
      level: "normal",
    };
    const token = jwt.sign(payload, "200000000"); // 20000 sec
    res.json({ jwt: token });
  } else {
    return res
      .status(403)
      .json({ auth: false, message: "Invalid refresh token" });
  }
});

module.exports = router;
