const OBS = require('esdk-obs-nodejs');
const uuidv4 = require('uuid');
const path = require('path');
const fs = require('fs-extra');

module.exports = {
  uploadFile:async function (files, folderName) {
    if (files != null) {
      let fileExtention = files.name.split('.').pop();
      let newFileName = `${uuidv4.v4()}.${fileExtention}`;
      let newpath = path.resolve(__dirname + `./../../uploaded/${folderName}`) + '/' + newFileName;

      await fs.move(files.tempFilePath, newpath);
      return newFileName;
    }
  }
}
